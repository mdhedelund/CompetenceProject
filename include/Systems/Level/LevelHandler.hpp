#pragma once

#include <string>
#include <fstream>

namespace Motherload
{
    class Game;
    class LevelHandler
    {
    private:
        const std::string lvl_filename = "level.lvl";
        int currentSeed;

    public:
        Motherload::Game* game;
        void SetSeed(int seed);
        void LoadLevel(std::string lvl_filename);
        void UnloadCurrentLevel();
        void SaveLevel(std::string lvl_filename);
    };
} // namespace Motherload