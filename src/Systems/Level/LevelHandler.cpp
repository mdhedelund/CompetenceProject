#include <Systems/Level/LevelHandler.hpp>

#include <Game.h>

namespace Motherload
{
    void LevelHandler::SetSeed(int seed) {
        currentSeed = seed;
    }

    void LevelHandler::LoadLevel(std::string lvl_filename)
    {
        UnloadCurrentLevel();
        std::ifstream level_file;
        level_file.open(lvl_filename);
        level_file >> currentSeed;
        level_file.close();
        game->populateBlockGrid(currentSeed);
        for (int i = 0; i < game->blocks.size(); i++)
        {
            for (int j = 0; j < game->blocks[i].size(); j++)
            {
                game->blocks[i][j]->initialize();
            }
        }
    }

    void LevelHandler::UnloadCurrentLevel()
    {
        for (int i = 0; i < game->blocks.size(); i++) 
        {
            for (int j = 0; j < game->blocks[i].size(); j++)
            {
                if (game->blocks[i][j]) {
                    game->destroyBlock(game->blocks[i][j]);
                }
            }
        }
        game->blocks.clear();
    }

    void LevelHandler::SaveLevel(std::string lvl_filename)
    {
        std::ofstream level_file;
        level_file.open(lvl_filename, std::ofstream::out | std::ofstream::trunc);
        level_file << currentSeed;
        level_file.close();
    }
}